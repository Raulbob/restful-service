package app.configuration;

import app.model.entity.User;
import app.service.DummyUserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "app")
public class AppConfig {


    @Bean
    public DummyUserService firstService() {
        DummyUserService service = new DummyUserService();
        service.add(new User(0, "emilia@gmail.com", "345678asdsad", "ADMIN"));
        service.add(new User(0, "george@gmail.com", "345678ac123sdsad", "SUPERVISOR"));
        service.add(new User(0, "enache@gmail.com", "345678ad216575sdsad", "ADMIN"));
        service.add(new User(0, "miriam@gmail.com", "34569999dsad", "USER"));
        return service;
    }

}
