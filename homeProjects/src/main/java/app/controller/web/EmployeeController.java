package app.controller.web;

import app.exception.EmployeeNotFoundException;
import app.model.dto.EmployeeDTO;
import app.model.entity.User;
import app.service.UserService;
import app.util.converter.EmployeeModelAssembler;
import app.util.converter.UserDTOConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class EmployeeController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDTOConverter converter;

    @Autowired
    private EmployeeModelAssembler assembler;

    //GET -> get all users
    @RequestMapping("/helloWorld")
    public String doSomething() {
        System.out.println("Primul meu Rest WebService ");
        int x= 13;
        int y = 13;
        int result = userService.doSomething(x, y);
        return "" + result;
    }
//
//    /**
//     * Return all employees.
//     */
//    @GetMapping("/employees")
//    public List<EmployeeDTO> all() {
//        return converter.toDTO(userService.getUsers());
//    }

    /**
     * Return all employees using HATEOAS.
     */
    @GetMapping("/employees")
    public CollectionModel<EntityModel<EmployeeDTO>> all() {
        List<EmployeeDTO> employees = (converter.toDTO(userService.getUsers()));
        List<EntityModel<EmployeeDTO>> entities = employees.stream().map(userDTO-> assembler.toModel(userDTO))
                .collect(Collectors.toList());

        return new CollectionModel<>(entities,
                linkTo(methodOn(EmployeeController.class).all()).withSelfRel());
    }



//    /**
//     * Find the employee with the given Id and return it.
//     */
//    @GetMapping("/employees/{id}")
//    public EmployeeDTO one(@PathVariable int id) {
//        User user  = userService.find(id);
//        if (user != null) {
//            return converter.toDTO(user);
//        } else {
//            throw new EmployeeNotFoundException(id);
//        }
//    }

        /**
     * Find the employee with the given Id and return it HTEOAS.
     */
    @GetMapping("/employees/{id}")
    public EntityModel<EmployeeDTO> one(@PathVariable int id) {
        User user  = userService.find(id);
        if (user != null) {
            return assembler.toModel(converter.toDTO(user));

        } else {
            throw new EmployeeNotFoundException(id);
        }
    }

    /**
     * Create a new employee.
     */
    @PostMapping("/employees")
    public EmployeeDTO add(@RequestBody User newEmployee) {
        return converter.toDTO(userService.add(newEmployee));
    }


//    /**
//     * Update a specific employee .
//     * @return
//     */
//    @PutMapping("/employees/{id}")
//    public EmployeeDTO replaceEmployee(@RequestBody User newEmployee, @PathVariable int id) {
//
//        User user = userService.find(id);
//        if (user != null) {
//            return converter.toDTO(userService.update(newEmployee));
//        } else {
//            return converter.toDTO(userService.add(newEmployee.getEmail(), newEmployee.getPassword(), newEmployee.getUserRole()));
//        }
//    }

    /**
     * Update a specific employee .
     * @return
     */
    @PutMapping("/employees/{id}")
    public EntityModel<EmployeeDTO> replaceEmployee(@RequestBody User newEmployee, @PathVariable int id) {
        User user = userService.find(id);
        if (user != null) {
            return assembler.toModel(converter.toDTO(userService.update(newEmployee)));
        } else {
            return assembler.toModel(converter.toDTO(userService.add(newEmployee.getEmail(), newEmployee.getPassword(), newEmployee.getUserRole())));
        }
    }

    /**
     * Delete the employee with the given Id.
     */
    @DeleteMapping("/employees/{id}")
    public boolean deleteEmployee(@PathVariable int id) {
       return userService.delete(id);
    }


}
