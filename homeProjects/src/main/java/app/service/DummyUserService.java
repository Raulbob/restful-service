package app.service;

import app.model.entity.User;

import java.util.ArrayList;
import java.util.List;

public class DummyUserService implements UserService {

    private List<User> users = new ArrayList<>();

    public User add(String email, String password, String userRole) {
        User user = new User(email, password, userRole);
        return add(user);
    }

    public User add(User user) {
        if (users.isEmpty()) {
            user.setId(0);
        } else {
            user.setId(users.size());
        }
        users.add(user);
        return user;
    }

    public List<User> getUsers() {
        return users;
    }

    public User find(int id) {
        if (id < users.size()) {
            return users.get(id);
        } else {
            return null;
        }
    }

    public User update(User user) {
        User oldUser = find(user.getId());
        if (oldUser != null) {
            oldUser.setEmail(user.getEmail());
            oldUser.setPassword(user.getPassword());
            oldUser.setUserRole(user.getUserRole());
            return oldUser;
        } else {
            return null;
        }
    }

    public boolean delete(int id) {
        if (id <users.size()) {
            users.remove(id);
            return true;
        } else {
            return false;
        }
    }

    public int doSomething(int x, int y) {
        return x*y;
    }







}
