package app.service;

import app.model.entity.User;

import java.util.List;

public class SQLUserService implements UserService {
    @Override
    public int doSomething(int x, int y) {
        return 0;
    }

    @Override
    public List<User> getUsers() {
        return null;
    }

    @Override
    public User add(User user) {
        return null;
    }

    @Override
    public User add(String email, String password, String userRole) {
        return null;
    }

    @Override
    public User find(int id) {
        return null;
    }

    @Override
    public User update(User user) {
        return null;
    }

    @Override
    public boolean delete(int id) {
        return false;
    }
}
