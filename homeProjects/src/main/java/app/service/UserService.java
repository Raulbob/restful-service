package app.service;

import app.model.entity.User;

import java.util.List;

public interface UserService {

    int doSomething(int x, int y);

    List<User> getUsers();

    User add(User user);

    User add(String email, String password, String userRole);

    User find(int id);

    User update(User user);

    boolean delete(int id);

}
