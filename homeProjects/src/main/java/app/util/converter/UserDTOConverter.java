package app.util.converter;

import app.model.dto.EmployeeDTO;
import app.model.entity.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserDTOConverter {

    public List<EmployeeDTO> toDTO(List<User> users) {
        List<EmployeeDTO> dtos = new ArrayList<>();
        users.forEach(user-> {
            dtos.add(toDTO(user));
        });
        return dtos;
    }

    public EmployeeDTO toDTO(User user) {
        EmployeeDTO dto = new EmployeeDTO();
        dto.setEmail(user.getEmail());
        dto.setId(user.getId());
        dto.setUserRole(user.getUserRole());
        return dto;
    }


}
